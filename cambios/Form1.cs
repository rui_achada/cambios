﻿namespace cambios
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Windows.Forms;
    using Newtonsoft.Json;
    using Modelos;
    using System.Threading.Tasks;
    using cambios.Servicos;
    using Newtonsoft.Json.Linq;

    public partial class Form1 : Form
    {
        #region Atributos

        private List<Rate> Rates;

        private NetworkService networkService;

        private ApiService apiService;

        private DialogService dialogService;

        private DataService dataService;

        #endregion

        //public List<Rate> Rates { get; set; } = new List<Rate>();

        public Form1()
        {
            InitializeComponent();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService(); // chama o construtor e trata logo da parte da pasta
            LoadRates();
        }

        private async void LoadRates()
        {
            bool load;

            labelResultado.Text = "A atualizar taxas...";

            var connection = networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                LoadLocalRates(); // Rates da base de dados
                load = false;
                //MessageBox.Show(connection.Message);
                //return;
            }
            else
            {
                await LoadApiRates();
                load = true;
            }

            if (Rates.Count == 0)
            {
                labelResultado.Text = "Não há ligação à internet" + Environment.NewLine +
                                      "e não foram previamente carregadas as taxas" + Environment.NewLine +
                                      "Tente mais tarde!";

                labelStatus.Text = "Primeira inicialização deverá ter ligação à Internet";
                return;
            }

            // https://stackoverflow.com/questions/27645952/json-net-cannot-deserialize-the-current-json-object-e-g-namevalue-in

            //var o = JsonConvert.DeserializeObject<Rate>(result);
            //List<Rate> lista = JsonConvert.DeserializeObject<List<Rate>>(result);

            //"{\"date\":\"2018-10-30\",\"rates\":[{\"BGN\":1.9558,\"CAD\":1.4941,\"BRL\":4.2282,\"HUF\":324.95,\"DKK\":7.4614,\"JPY\":128.28,\"ILS\":4.2159,\"TRY\":6.2613,\"RON\":4.667,\"GBP\":0.89148,\"PHP\":60.862,\"HRK\":7.4315,\"NOK\":9.5435,\"USD\":1.1372,\"MXN\":22.7513,\"AUD\":1.6025,\"IDR\":17301.42,\"KRW\":1295.91,\"HKD\":8.9229,\"ZAR\":16.6291,\"ISK\":137.7,\"CZK\":25.87,\"THB\":37.88,\"MYR\":4.7558,\"NZD\":1.7349,\"PLN\":4.3307,\"SEK\":10.42,\"RUB\":74.6963,\"CNY\":7.9203,\"SGD\":1.5739,\"CHF\":1.1386,\"INR\":83.728}],\"base\":\"EUR\"}";

            //"[{\"RateId\":1,\"Code\":\"AED\",\"TaxRate\":3.673018,\"Name\":\"United Arab Emirates\"},{\"RateId\":1,\"Code\":\"AED\",\"TaxRate\":3.673018,\"Name\":\"United Arab Emirates\"},{\"RateId\":1,\"Code\":\"AED\",\"TaxRate\":3.673018,\"Name\":\"United Arab Emirates\"},{\"RateId\":1,\"Code\":\"AED\",\"TaxRate\":3.673018,\"Name\":\"United Arab Emirates\"}]";

            comboBoxOrigem.DataSource = Rates;
            comboBoxOrigem.DisplayMember = "Name";
            //comboBoxOrigem.ValueMember = "rates";

            comboBoxDestino.BindingContext = new BindingContext();

            comboBoxDestino.DataSource = Rates;
            comboBoxDestino.DisplayMember = "Name";

            labelResultado.Text = "Taxas atualizadas...";

            if (load)
            {
                labelStatus.Text = string.Format("Taxas carregadas da internet em {0:F}", DateTime.Now);
            }
            else
            {
                labelStatus.Text = string.Format("Taxas carregadas da base de dados.");
            }

            progressBar1.Value = 100;
            buttonConverter.Enabled = true;
            buttonTroca.Enabled = true;
        }

        private void LoadLocalRates()
        {
            Rates = dataService.GetData();
        }

        private async Task LoadApiRates()
        {
            progressBar1.Value = 0;

            var response = await apiService.getRates("https://api.exchangeratesapi.io",
                "/latest");

            Rates = (List<Rate>) response.Result;

            dataService.DeleteData();

            dataService.SaveData(Rates);
        }

        private void buttonConverter_Click(object sender, EventArgs e)
        {
            Converter();
        }

        private void Converter()
        {
            if (string.IsNullOrEmpty(textBoxValor.Text))
            {
                dialogService.ShowMessage("Erro", "Insira um valor a converter");
                return;
            }

            decimal valor;
            if (!decimal.TryParse(textBoxValor.Text, out valor))
            {
                dialogService.ShowMessage("Erro de conversão", "Valor terá que ser numérico");
                return;
            }

            if (comboBoxOrigem.SelectedItem == null)
            {
                dialogService.ShowMessage("Erro", "Tem que escolher uma moeda de origem a converter");
                return;
            }

            if (comboBoxDestino.SelectedItem == null)
            {
                dialogService.ShowMessage("Erro", "Tem que escolher uma moeda de destino a converter");
                return;
            }

            var taxaOrigem = (Rate) comboBoxOrigem.SelectedItem;

            var taxaDestino = (Rate)comboBoxDestino.SelectedItem;

            var valorConvertido = valor / (decimal) taxaOrigem.TaxRate * (decimal) taxaDestino.TaxRate;

            labelResultado.Text = string.Format("{0} {1:C2} = {2} {3:C2}", taxaOrigem.Code, valor, taxaDestino.Code,
                valorConvertido);

        }

        private void buttonTroca_Click(object sender, EventArgs e)
        {
            Troca();
        }

        private void Troca()
        {
            var aux = comboBoxOrigem.SelectedItem;

            comboBoxOrigem.SelectedItem = comboBoxDestino.SelectedItem;
            comboBoxDestino.SelectedItem = aux;
            Converter();
        }
    }
}
