﻿namespace cambios.Servicos
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Modelos;

    public class ApiService
    {
        public async Task<Response> getRates(string urlBase, string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);

                var response = await client.GetAsync(controller);

                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }

                // api local (ignora as linhas anteriores)
                var jsonData = "[{\"RateId\":1,\"Code\":\"AED\",\"TaxRate\":3.673018,\"Name\":\"United Arab Emirates\"},{\"RateId\":2,\"Code\":\"AAA\",\"TaxRate\":4.673018,\"Name\":\"NomeAAA\"},{\"RateId\":3,\"Code\":\"BBB\",\"TaxRate\":5.673018,\"Name\":\"NomeBBB\"},{\"RateId\":4,\"Code\":\"CCC\",\"TaxRate\":6.673018,\"Name\":\"NomeCCC\"}]";

                //var rates = JsonConvert.DeserializeObject<List<Rate>>(result); --> API do site
                var rates = JsonConvert.DeserializeObject<List<Rate>>(jsonData);

                return new Response
                {
                    IsSuccess = true,
                    Result = rates,
                };
            }

            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }
    }
}
